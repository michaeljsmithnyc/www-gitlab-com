---
layout: handbook-page-toc
title: Transitioning Accounts from Pre-Sales to Post-Sales
description: >-
  How to effectively transition a customer from the pre-sales engagement to post-sales to ensure the customer is successful.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

When a prospect is moving towards becoming a customer, they will need to be transitioned from the pre-sales engagement model to post-sales. In practice, that means that the Account Executive/Solutions Architect works with the Technical Account Manager to facilitate a smooth handoff and ensure continuity for the customer. 

  - **Commercial:** AE or SA will facilitate the handoff to TAM
  - **Enterprise:** SA will facilitate the handoff to TAM

## Transition timeline

| Team | Stage | Notes | 
| ---  | ---   | ---   |
| Commercial | `Closed Won` | Once a prospect opportunity reaches [stage ](/handbook/sales/field-operations/gtm-resources/#opportunity-stages)`Closed-Won`, the AE/SA and TAM should begin the transition process. The AE/SA will send an email to the technical customer team to introduce the TAM.| 
| Enterprise | `5-Negotiating` or <br> `Closed Won`| Once a prospect opportunity reaches [stage ](/handbook/sales/field-operations/gtm-resources/#opportunity-stages) `5-Negotiating` or `Closed Won`, the SA and TAM should begin the transition process. The SA will set up the call with the technical customer team to introduce the TAM.| 

The transition process is outlined in the sections below. 

Most of the process outlined below should occur between the opportunity reaching stage `5-Negotiating` and `Closed Won`.

### Prior to the sale

TAM engagement prior to the sale should occur in the following situations:

  - At the POV completion & presentation meeting when the results are reviewed and the next steps agreed upon
  - A shared customer issue tracking project has been created that will affect the account long-term
  - As requested by the SA if the TAM has a specific subject matter expertise relevant to the conversation

### Internal account team discussion

In preparation for introducing the TAM, the [account team](/handbook/customer-success/account-team/) should schedule a meeting to discuss general information about the customer, to include:

- Account name and industry/sector
- Key people
  - Champion
  - Decision maker/economic buyer
  - Main points of contact
- Command Plan and reasons for purchasing GitLab
  - Defined business outcomes
  - Pain points
  - Metrics
  - Impediments to adoption and known risks
- POV overview
  - Technical objectives and results
  - Pending items to resolve
  - Customer's required capabilities
  - Overall sentiment coming out of the POV
- Services engagement
  - Scope and deliverables
  - Timeline

If the Solutions Architect created a collaboration project, the TAM and SA should review it together, and ensure that `readme` details and issues are up to date.

### TAM introduction to the customer

At the conclusion of the pre-sales engagement, the Account Executive or Solutions Architect should set up an introduction to the TAM, provide the customer an overview of the TAM role, and what the TAM engagement will look like going forward.

For Enterprise customers, the TAM should work with the SA to prepare the [TAM program overview deck](https://docs.google.com/presentation/d/1n_Tex7gm8_UgxEaUy8YR3wccb73bsWOugdQ9mQX_oMU/edit?usp=sharing) to be presented as part of the meeting. This will be starting point of the TAM's relationship with the customer, and it's important to convey what the TAM's role is, and the expectations from both sides. As this is part of the SA's review with the customer, the TAM portion should not be extensive and should be used to set the stage for future conversations.

For Commercial customers, the AE/SA will need to ensure that Command Plan and Pitch Deck (R7) are complete before scheduling an internal transition meeting to brief the TAM. AE/SA will then introduce the TAM via email. 

From this initial introduction, the TAM should schedule the kickoff meeting with the customer to go into more detail about the customer's objectives and the TAM program. Please [review the details for the TAM kickoff call](/handbook/customer-success/tam/onboarding/#kickoff-call) to learn more about how to conduct this meeting.


| | Commercial | Enterprise | 
| ---  | ---   | ---   |
| **Who** will coordinate the TAM introduction | SA/AE | SA | 
| **When** will the introduction happen | `Closed Won` | `5-Negotiating` or `Closed Won` |
| **Fromat** of the introduction | SA/AE will send an introduction email to the technical team on the customer end.| SA will schedule a 1 hour call with the technical team on the customer end. | 
| **Where** can the TAM access account information | - Custom Pitch Deck <br> - Command Plan <br> - POV (when applicable) | - Account plan POV <br> - Command Plan <br> - SA-TAM existing best practices | 


### Shifting responsibility from AE/SA to TAM

Once the account is in post-sales, the TAM takes primary responsibility for guidance & best practices conversations, customer enablement, and product usage. Most responsibilities of the AE/SA will transition to the TAM at this point.

During customer onboarding, the SA may choose to stay engaged with the customer to help facilitate a seamless handoff and address any ongoing activities from the POV. This should be done in such a way as to allow the TAM to take over those activities, so all conversations with the customer on these items should include the TAM.

Once the account is fully transitioned to post-sales, the SA may be invited by the TAM for the following activities:

- POV for an additional business unit or a license tier upgrade
- Professional Services scoping and discovery for a [Statement of Work](/handbook/customer-success/professional-services-engineering/working-with/#custom-services-sow-creation-and-approval)
- Support of Lunch & Learns, GitLab Days, and on-site evangelism opportunities
- Executive Business Reviews (EBRs)

The SA may also be involved, as needed and at the discretion of the SA & their manager, to support customer enablement, EBR delivery, product demo, or other customer-facing activities for which the TAM may ask for assistance.

### Reengaging the SAL/AE and SA

There are also instances where the TAM will need to reengage the SAL/AE and SA.  Here are a few examples of customer requests the TAM can listen for in order to reengage them: 

  - Development of expansion plans with the SAL and TAM leading to tier upgrades
  - A POV, product evaluation, RFP, or security begins for a "new" single team or an enterprise-wide tier upgrade not for renewal purposes
  - SAs may assist TAMs with an existing customer team on an exception basis. SA leadership approval is required.
    - Customer enablement assistance when specifically requested by the TAM team
    - Workshops assistance when specifically requested by the TAM team
    - ROI analysis for an existing customer team when requested by the TAM team
    - Unless it's for a tier upgrade, presales activities such as POVs and RFPs should be avoided for an existing customer team. Like with any presales opportunity, these activities should not be offered as a sales tools, but leveraged when a prospect/customer requires these activities as part of their evaluation.
  - Any Professional Services that are being discussed and may require an SOW
  - If a TAM is over-committed or unable to support a customer request, the SA may be requested to assist
  - Support of GitLab days or other on-site evangelism of GitLab at customer sites
  - Executive Business Reviews (EBRs)

  [us-public-sector](https://gitlab.com/gitlab-com/us-public-sector) parent project for all public sector work

  [account-management](https://gitlab.com/gitlab-com/account-management/) parent project for account-specific work and collaboration with the rest of the sales

  [customer-success](https://gitlab.com/gitlab-com/customer-success) parent project for Customer Success employee, shared or triage work

  


