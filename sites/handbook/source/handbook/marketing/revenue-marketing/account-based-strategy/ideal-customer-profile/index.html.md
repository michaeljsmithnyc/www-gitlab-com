---
layout: handbook-page-toc
title: "Ideal Customer Profile"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Ideal Customer Profile
An ideal customer profile is the description of our "perfect" customer (company, not individual or end user).  The profile takes into consideration firmographic, environmental and additional factors to develop our focus list of highest value accounts.


### Large

|  | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of developers | 500+ |
| | Tech stack (regional) | Includes GitHub, Perforce, Jenkins, BitBucket or Subversion |
| | Cloud provider | AWS or GCP |
| | Prospect | First order logo (not a current PAID customer for GitLab anywhere within the organization).  This is defined [here](https://about.gitlab.com/handbook/sales/sales-term-glossary/#first-order-customers).  |
| **Additional criteria (attributes to further define)** | Digital transformation | identified C-suite initiative | 


### Mid Market 

| 500+ employees | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of employees | >500 |
| **Core criteria (must haves)** | Number of developers | 100+ |
| | Tech stack (regional) | Includes GitHub, Perforce, Jenkins, BitBucket, Bamboo, or Subversion OR the absence of a tech stack. |
| | Prospect | First order logo (not a current PAID customer for GitLab anywhere within the organization).  This is defined [here](https://about.gitlab.com/handbook/sales/sales-term-glossary/#first-order-customers).  |
| **Additional criteria (attributes to further define)** | New hire | CIO | 


| <500 employees | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of employees | <500 |
| | Tech stack (regional) | A lack of technology installed.  GitLab would be the first purchsed for Devops for these companies. |
| | Prospect | First order logo (not a current PAID customer for GitLab anywhere within the organization).  This is defined [here](https://about.gitlab.com/handbook/sales/sales-term-glossary/#first-order-customers).  |
| **Additional criteria (attributes to further define)** | New hire | CIO | 

