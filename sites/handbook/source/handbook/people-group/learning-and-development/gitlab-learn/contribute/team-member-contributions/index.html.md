---
layout: handbook-page-toc
title: Become a GitLab Learning Evangelist
description: "Contribution process for GitLab team members"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Contributing to GitLab Learn as a Team Member

Team members have two paths they can follow to contribute learning content to the GitLab Learn platform. Review these scenarioes to decide which role best fits your needs. Scroll down on this page to learn more about each role.

| Scenario | Contributor Role |
| ----- | ----- |
| I want to make suggestions to team members on what existing learning material might be best for their professional development journey. For example, I want to pull together some LinkedIn Learning courses that would help them develop new skills | [Become a Learning Curator](https://gitlab.edcast.com/pathways/ECL-5bcdc812-5b76-4921-b483-5846ba5acd79) |
| I'd like you create a brand new learning pathway for team members or the wider community. I plan on creating my own content, recording videos, designing handbook pages, and more to deliver this course | [Become a Learning Evangelist](https://gitlab.edcast.com/pathways/ECL-f9be1e50-ba17-46b3-af33-731d19b3ffcd) |

## Become a GitLab Learning Curator

Curators in GitLab Learn can organize existing learning content in their specific team Groups or Channels to broadcast, organize, and encourage professional development. This role is perfect for PBPs, leaders, or managers who'd like to make suggestions to their team regarding what LinkedIn Learning courses might be most beneficial!

### GitLab Learn Curator Pathway

Earn the [Learning Curator badge in GitLab Learn](https://gitlab.edcast.com/pathways/ECL-5bcdc812-5b76-4921-b483-5846ba5acd79)

Upon completion of this learning path, GitLab Team members will earn the Learning Curator badge and be granted platform permissions that will allow them to curate learning content.


### Add the `Curator` role to your EdCast profile

Follow the steps outlined in the handbook to open an [individual access request for the `Curator` role in EdCast](/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/). First assign this AR to your manager for approval and to alert your manager that you've taken the relevant pathway and will be curating content for your team. After approval from your manager, please assign the AR to `@slee24`.

Once processed, you're ready to start curating content on your team's channel! Please reach out in the [#learninganddevelopment Slack channel](https://app.slack.com/client/T02592416/CMRAWQ97W) if you need additional support.



## Become a GitLab Learning Evangelist

The Learning and Development team has created an opportunity for GitLab Team Members to become learning content contributors, called Learning Evangelists, in the GitLab Learn platform.

We've created a learning path to become a GitLab Learning Evangelist to

1. Ensure that all content contributors are trained on the EdCast platform
1. Communicate how handbook first is applied to e-learning content 
1. Ensure that content across the platform is cohesive and avoids repetition
1. Democratize the learning process and provide space for all voices to be heard
1. Keep content and look and feel of the platform aligned with overall brand

**Why become a Learning Evangelist?**

Learning Evangelists in GitLab Learn have the opportunity to:

1. Be the face of learning for your team and organization
1. Dedicate your passion for learning and developing others by contributing content to the LXP
1. Influence team members to take time out to learn new skills, build courses for the community, and feature training material for all to consume!


### GitLab Learning Evangelist Learning Pathway

Earn the [Learning Evangelist badge GitLab Learn](https://gitlab.edcast.com/pathways/learning-evangelist-training).

Upon completion of this learning path, GitLab Team members will earn the Learning Evangelist badge and be granted platform permissions that will allow them to contribute learning content.

### Add the `Learning Evangelist` role to your EdCast profile

Follow the steps outlined in the handbook to open an [individual access request for the `Learning Evangelist` role in EdCast](/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/). First assign this AR to your manager for approval and to alert your manager that you've taken the relevant pathway and will be creating and contributing learning content for your team. After approval from your manager, please assign the AR to `@slee24`. and assign the AR to `@slee24`.

Once processed, you're ready to start contributing content to GitLab Learn! Please reach out in the [#learninganddevelopment Slack channel](https://app.slack.com/client/T02592416/CMRAWQ97W) if you need additional support.


### After earning the Learning Evangelist Badge

After you've passed the final learning assessment and earned the GitLab Learning Evangelist badge, you're ready to start contributing to GitLab Learn!

The Learning and Development team will monitor your progress on the learning path and update your platform permissions when you've earned the badge. If you've completed the learning path but have not had your permissions updated yet, please reach out to the L&D team in Slack for support.


## FAQ from Learning Evangelists

1. Is there a required amount of content I need to contribute?
1. Can I access Articulate 360 or other authoring tools?
1. Where should I go if I have questions or need support with the GitLab Learn platform?
